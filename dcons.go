package dcons // import "go.thi.ng/dcons"

import (
	"fmt"
	"strings"
)

type Any = interface{}

type Cell struct {
	Prev  *Cell
	Next  *Cell
	Value Any
}

type DCons struct {
	Head *Cell
	Tail *Cell
	n    int
}

func New() *DCons {
	return &DCons{nil, nil, 0}
}

func NewWithValues(vals []Any) *DCons {
	l := &DCons{nil, nil, 0}
	for i := range vals {
		l.Push(vals[i])
	}
	return l
}

func (l *DCons) Copy() *DCons {
	res := New()
	cell := l.Head
	for n := l.n; n > 0; n-- {
		res.Push(cell.Value)
		cell = cell.Next
	}
	return res
}

func (l *DCons) Len() int {
	return l.n
}

func (l *DCons) Cons(v Any) *DCons {
	c := &Cell{Prev: nil, Next: l.Head, Value: v}
	if l.Head != nil {
		l.Head.Prev = c
	} else {
		l.Tail = c
	}
	l.Head = c
	l.n++
	return l
}

func (l *DCons) Push(v Any) *DCons {
	if l.Tail != nil {
		c := &Cell{Prev: l.Tail, Next: nil, Value: v}
		l.Tail.Next = c
		l.Tail = c
		l.n++
		return l
	}
	return l.Cons(v)
}

func (l *DCons) Drop() Any {
	c := l.Head
	if c == nil {
		panic("can't drop head from empty list")
	}
	l.Head = c.Next
	if l.Head != nil {
		l.Head.Prev = nil
	} else {
		l.Tail = nil
	}
	l.n--
	return c.Value
}

func (l *DCons) Pop() Any {
	c := l.Tail
	if c == nil {
		panic("can't pop tail from empty list")
	}
	l.Tail = c.Prev
	if l.Tail != nil {
		l.Tail.Next = nil
	} else {
		l.Head = nil
	}
	l.n--
	return c.Value
}

func (l *DCons) Remove(cell *Cell) *DCons {
	if cell.Prev != nil {
		cell.Prev.Next = cell.Next
	} else {
		l.Head = cell.Next
	}
	if cell.Next != nil {
		cell.Next.Prev = cell.Prev
	} else {
		l.Tail = cell.Prev
	}
	l.n--
	return l
}

func (l *DCons) InsertBefore(cell *Cell, val Any) *DCons {
	if cell == nil {
		panic("call can't be nil")
	}
	newCell := &Cell{Prev: cell.Prev, Next: cell, Value: val}
	if cell.Prev != nil {
		cell.Prev.Next = newCell
	} else {
		l.Head = newCell
	}
	cell.Prev = newCell
	l.n++
	return l
}

func (l *DCons) InsertAfter(cell *Cell, val Any) *DCons {
	if cell == nil {
		panic("call can't be nil")
	}
	newCell := &Cell{Prev: cell, Next: cell.Next, Value: val}
	if cell.Next != nil {
		cell.Next.Prev = newCell
	} else {
		l.Tail = newCell
	}
	cell.Next = newCell
	l.n++
	return l
}

func (l *DCons) InsertAt(n int, val Any) *DCons {
	cell := l.NthCell(n)
	if cell == nil {
		panic("index out of range")
	}
	return l.InsertBefore(cell, val)
}

func (l *DCons) IsCircular() bool {
	return l.Head != nil && l.Head.Prev == l.Tail
}

func (l *DCons) Circular() *DCons {
	if l.Tail != nil {
		l.Head.Prev = l.Tail
		l.Tail.Next = l.Head
	}
	return l
}

func (l *DCons) Nth(n int, notFound Any) Any {
	c := l.NthCell(n)
	if c != nil {
		return c.Value
	}
	return notFound
}

func (l *DCons) NthCell(n int) *Cell {
	if n < 0 {
		n += l.n
	}
	if n < 0 || n >= l.n {
		return nil
	}
	var c *Cell
	if n <= l.n/2 {
		c = l.Head
		for ; n > 0 && c != nil; n-- {
			c = c.Next
		}
	} else {
		c = l.Tail
		n = l.n - n - 1
		for ; n > 0 && c != nil; n-- {
			c = c.Prev
		}
	}
	return c
}

func (l *DCons) First() Any {
	if l.Head != nil {
		return l.Head.Value
	}
	return nil
}

func (l *DCons) Peek() Any {
	if l.Tail != nil {
		return l.Tail.Value
	}
	return nil
}

func (l *DCons) Find(val Any) *Cell {
	for cell := l.Head; cell != nil; {
		if cell.Value == val {
			return cell
		}
		cell = cell.Next
		if cell == l.Head {
			break
		}
	}
	return nil
}

func (l *DCons) FindWith(fn func(v Any) bool) *Cell {
	for cell := l.Head; cell != nil; {
		if fn(cell.Value) {
			return cell
		}
		cell = cell.Next
		if cell == l.Head {
			break
		}
	}
	return nil
}

func (l *DCons) Swap(a, b *Cell) *DCons {
	if a != nil && b != nil && a != b {
		t := a.Value
		a.Value = b.Value
		b.Value = t
	}
	return l
}

func (l *DCons) SwapNth(a, b int) *DCons {
	return l.Swap(l.NthCell(a), l.NthCell(b))
}

func (l *DCons) Slice(from, to int) *DCons {
	a := from
	b := to
	if a < 0 {
		a += l.n
	}
	if b < 0 {
		b += l.n
	}
	if a < 0 || b < 0 {
		panic(fmt.Sprintf("invalid indices: %d / %d", from, to))
	}
	res := New()
	cell := l.NthCell(a)
	for a++; cell != nil && a <= b; a++ {
		res.Push(cell.Value)
		cell = cell.Next
	}
	return res
}

func (l *DCons) Splice(at *Cell, del int, insert ...Any) *DCons {
	var res *DCons
	cell := at
	if del > 0 {
		res = New()
		for ; cell != nil && del > 0; del-- {
			l.Remove(cell)
			res.Push(cell.Value)
			cell = cell.Next
		}
	} else {
		cell = cell.Next
	}
	if len(insert) > 0 {
		if cell != nil {
			for _, v := range insert {
				l.InsertBefore(cell, v)
			}
		} else {
			for _, v := range insert {
				l.Push(v)
			}
		}
	}
	return res
}

// Do applies given function `fn` once to each cell and stops iterating
// if `fn` returns false.
func (l *DCons) Do(fn func(cell *Cell) bool) {
	for cell := l.Head; cell != nil; {
		if !fn(cell) {
			return
		}
		cell = cell.Next
		if cell == l.Head {
			return
		}
	}
}

func (l *DCons) Map(fn func(val Any) Any) *DCons {
	mapped := New()
	for cell := l.Head; cell != nil; {
		mapped.Push(fn(cell.Value))
		cell = cell.Next
		if cell == l.Head {
			break
		}
	}
	return mapped
}

func (l *DCons) ToSlice() []Any {
	n := l.n
	arr := make([]Any, n, n)
	cell := l.Head
	for i := 0; i < n; i++ {
		arr[i] = cell.Value
		cell = cell.Next
	}
	return arr[:]
}

func (l *DCons) String() string {
	str := &strings.Builder{}
	str.WriteString("DCons[")
	cell := l.Head
	for i, n := 0, l.n-1; i <= n; i++ {
		fmt.Fprint(str, cell.Value)
		if i < n {
			str.WriteRune(' ')
		}
		cell = cell.Next
	}
	str.WriteRune(']')
	return str.String()
}
